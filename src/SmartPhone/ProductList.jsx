import React, { Component } from "react";
import ProductItem from "./ProductItem";
import { connect } from "react-redux";

class ProductList extends Component {
  renderProduct = () => {
    return this.props.products.map((item) => {
      return (
        <div className="col-3 mt-2" key={item.id}>
          <ProductItem product={item} />
        </div>
      );
    });
  };
  render() {
    return (
      <div className="container-fluid">
        <div className="row">{this.renderProduct()}</div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    products: state.productList.products,
  };
};
export default connect(mapStateToProps)(ProductList);
