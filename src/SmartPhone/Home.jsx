import React, { Component } from "react";
import Cart from "./Cart";
import Detail from "./Detail";
import ProductList from "./ProductList";

class Home extends Component {
  state = {
    show: false,
  };
  handlebtn = () => {
    this.setState({
      show: !this.state.show,
    });
  };
  render() {
    return (
      <div className="container text-center">
        <h1>SmartPhone Store</h1>
        <button
          className="bg-danger p-2 text-white border rounded-lg"
          onClick={this.handlebtn}
        >
          Giỏ hàng
        </button>
        {this.state.show ? <Cart hadel={this.handlebtn} /> : ""}
        <ProductList />
        <Detail />
      </div>
    );
  }
}

export default Home;
