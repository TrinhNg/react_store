import React, { Component } from "react";
import { connect } from "react-redux";

class Detail extends Component {
  render() {
    const { desc, backCamera, frontCamera, img, screen, name } =
      this.props.select;
    return (
      <div
        className={this.props.select.id ? "container text-center" : "d-none"}
      >
        <div className="row mt-5">
          <div className="col-5">
            <h4 className="mb-4">{name}</h4>
            <img src={img} className="w-100" alt="product" />
          </div>
          <div className="col-7">
            <h5 className="mb-5">THÔNG SỐ KỸ THUẬT</h5>
            <table className="table mt-3">
              <tbody>
                <tr>
                  <td>Màn hình </td>
                  <td>{screen}</td>
                </tr>
                <tr>
                  <td className="w-25"> Camera trước</td>
                  <td>{frontCamera}</td>
                </tr>
                <tr>
                  <td> Camera sau </td>
                  <td>{backCamera}</td>
                </tr>
                <tr>
                  <td>Mô tả </td>
                  <td>{desc}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return { select: state.productList.seclecPro };
};
export default connect(mapStateToProps)(Detail);
