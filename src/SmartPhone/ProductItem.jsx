import React, { Component } from "react";
import { connect } from "react-redux";

class ProductItem extends Component {
  handelDetail = (idDe) => {
    this.props.dispatch({
      type: "SET_DETAIL",
      payload: idDe,
    });
  };
  handelCart = (idCa) => {
    this.props.dispatch({
      type: "SET_CART",
      payload: idCa,
    });
  };
  render() {
    const { img, name, desc } = this.props.product;
    return (
      <div className="card h-100 text-center">
        <img src={img} className="w-75 h-50" alt="ngan sat thu" />
        <div>
          <h5 className="mt-2">{name}</h5>
          <p>{desc}</p>
        </div>
        <div className="card-body d-flex mx-1 text-white">
          <button
            className="mr-2 btn btn-info"
            onClick={() => this.handelDetail(this.props.product)}
          >
            Chi tiết
          </button>
          <button
            className="btn btn-danger"
            onClick={() => this.handelCart(this.props.product)}
          >
            Thêm hàng
          </button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    producr: state.productList.products,
    select: state.productList.seclecPro,
    selectCar: state.productList.selecCar,
  };
};
export default connect(mapStateToProps)(ProductItem);
