import React, { Component } from "react";
import { connect } from "react-redux";

class Cart extends Component {
  reduceCar = (id) => {
    this.props.dispatch({
      type: "SET_REDUCE",
      payload: id,
    });
  };
  addCar = (id) => {
    this.props.dispatch({
      type: "SET_ADD",
      payload: id,
    });
  };
  deleteCar = (id) => {
    this.props.dispatch({
      type: "SET_DELETE",
      payload: id,
    });
  };
  renderCart = () => {
    if (this.props.selectCar.length === 0) {
      return this.props.hadel();
    }
    return this.props.selectCar.map((item) => {
      const { id, name, img, price, quantity } = item;
      return (
        <tr key={id}>
          <td>{id}</td>
          <td>
            <img style={{ width: 120 }} src={img} alt="ngan sat thu" />
          </td>
          <td>{name}</td>
          <td>
            <button className="btn btn-info" onClick={() => this.reduceCar(id)}>
              -
            </button>
            <span>{quantity}</span>
            <button className="btn btn-info" onClick={() => this.addCar(id)}>
              +
            </button>
          </td>
          <td>{price}</td>
          <td>{quantity * price}</td>
          <td>
            {" "}
            <button
              onClick={() => {
                this.deleteCar(id);
              }}
              className="bg-danger btn"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div
        className="modal d-block"
        id="exampleModal"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-xl" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Giỏ hàng</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true"></span>
              </button>
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <th>Mã sp</th>
                    <th>Hình ảnh</th>
                    <th>Tên</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th>Thành tiền</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>{this.renderCart()}</tbody>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
                onClick={this.props.hadel}
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Total
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    selectCar: state.productList.selecCar,
    // selectCar: state.productList.selecCar,
  };
};
export default connect(mapStateToProps)(Cart);
