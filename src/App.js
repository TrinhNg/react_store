import Home from "./SmartPhone/Home";

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
