const productList = {
  products: [
    {
      id: "sp_1",
      name: "iphoneX",
      price: 30000,
      screen: "screen_1",
      backCamera: "backCamera_1",
      frontCamera: "frontCamera_1",
      img: "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg",
      desc: "iPhone X features a new all-screen design. Face ID, which makes your face your password",
    },
    {
      id: "sp_2",
      name: "Note 7",
      price: 20000,
      screen: "screen_2",
      backCamera: "backCamera_2",
      frontCamera: "frontCamera_2",
      img: "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
      desc: "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
    },
    {
      id: "sp_3",
      name: "Vivo",
      price: 10000,
      screen: "screen_3",
      backCamera: "backCamera_3",
      frontCamera: "frontCamera_3",
      img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
      desc: "A young global smartphone brand focusing on introducing perfect sound quality",
    },
    {
      id: "sp_4",
      name: "Blacberry",
      price: 15000,
      screen: "screen_4",
      backCamera: "backCamera_4",
      frontCamera: "frontCamera_4",
      img: "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
      desc: "BlackBerry is a line of smartphones, tablets, and services originally designed",
    },
  ],
  seclecPro: {},
  selecCar: [],
};

const reducer = (state = productList, action) => {
  switch (action.type) {
    case "SET_DETAIL":
      state.seclecPro = action.payload;
      return { ...state };
    case "SET_CART":
      const car = state.selecCar.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (car === -1) {
        action.payload.quantity = 1;
        state.selecCar.push(action.payload);
      } else {
        state.selecCar[car].quantity++;
      }
      return { ...state };
    case "SET_REDUCE":
      let cloneState = [...state.selecCar];
      const cart = cloneState.findIndex((item) => {
        return item.id === action.payload;
      });
      if (cloneState[cart].quantity > 1) {
        cloneState[cart].quantity--;
      } else {
        cloneState.splice(cart, 1);
      }
      return {
        ...state,
        selecCar: cloneState,
      };
    case "SET_ADD":
      const clone = [...state.selecCar];
      const addcart = clone.findIndex((item) => {
        return item.id === action.payload;
      });
      clone[addcart].quantity++;
      return {
        ...state,
        selecCar: clone,
      };
    case "SET_DELETE":
      const clones = [...state.selecCar];
      const carts = clones.findIndex((item) => {
        return item.id === action.payload;
      });
      clones.splice(carts, 1);
      return {
        ...state,
        selecCar: clones,
      };
    default:
      return state;
  }
};

export default reducer;
